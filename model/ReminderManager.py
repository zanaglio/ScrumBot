# coding: utf-8

import json
import threading, time
import datetime

from time import strftime
from slackclient import SlackClient

from model.DataTool import DataTool

class ReminderManager:


    REMINDER_FILENAME = "model/data/reminder.json"

    REMIND_REGEX = "rappel\s((<@(|[WU].+?)>\s?)*)(.*)(à\s([0-9][0-9]?h[0-5][0-9]?))\s?(tous\sles\sjours)?"

    REMINDER_DICTIONNARY = None

    REMIND_THREAD = None

    SLACK_CLIENT = None

    DATA_TOOL = DataTool(REMINDER_FILENAME)

    @staticmethod
    def init_reminder_process(slack_client):

        ReminderManager.SLACK_CLIENT = slack_client

        ReminderManager.REMIND_THREAD = threading.Thread(target=ReminderManager.remind_loop)
        ReminderManager.REMIND_THREAD.daemon = True
        ReminderManager.REMIND_THREAD.start()


    @staticmethod
    def remind_loop():

        while True:
            time_now = strftime("%Hh%M", time.localtime())
            reminders = ReminderManager.get_reminder_by_hour(time_now)
            for key in reminders:

                reminder = reminders[key]
                for user in reminder["users"]:

                    # Sends the response back to the channel
                    author = "<@" + reminder["author"].encode("utf-8", errors="ignore").decode('utf-8') + ">"
                    remind_content = str(reminder["content"].encode("utf-8", errors="ignore").decode('utf-8'))
                    channel = user.replace('<', '').replace('>', '')
                    print(author + " " + remind_content + " " + channel)
                    ReminderManager.remind_something_at_someone(author, remind_content, channel)

                if not reminder["is_everyday"]:
                    ReminderManager.DATA_TOOL.delete_key(key)

            time.sleep(1)

    @staticmethod
    def create_reminder(regexp_matches, author):

        users_mentionned = regexp_matches.group(1).split(" ")[0:-1]
        content = regexp_matches.group(4)[0:100].strip()
        hour = datetime.datetime.strptime(regexp_matches.group(6), "%Hh%M").strftime("%Hh%M")
        is_everyday = True if regexp_matches.group(7) else False

        author_reminder = ReminderManager.get_reminder_by_author(author)
        response_to_return = ""
        reminder_already_exists = False

        for key in author_reminder:
            reminder = author_reminder[key]
            if reminder["content"] == content and reminder["hour"] == hour and reminder["users"] == users_mentionned:
                reminder_already_exists = True
                break

        if reminder_already_exists:
            people = " cette personne "
            if len(users_mentionned) > 1:
                people = " ces personnes "
            response_to_return = "Vous avez déjà configuré ce rappel pour" + people + ":confused:"

        else:
            response_to_return = "Je vais rappeler à " + (','.join(users_mentionned)) + ": " + content.strip() + " à " + hour + " " + ("tous les jours" if is_everyday else "")
            ReminderManager.DATA_TOOL.add_key({
                "author": author,
                "users": users_mentionned,
                "content": content,
                "hour": hour,
                "is_everyday": is_everyday
            })

        return response_to_return

    @staticmethod
    def remove_reminder_by_id(reminder_id):

        # Remove a reminder by its id
        result = ReminderManager.DATA_TOOL.delete_key(reminder_id)
        return result

    @staticmethod
    def get_regexp_validator():
        return ReminderManager.REMIND_REGEX

    @staticmethod
    def get_reminder_by_author(author):

        reminders = {}
        dictionary = ReminderManager.DATA_TOOL.get_dictionary()
        # Retrieve all the reminders of the given author
        for key in dictionary:

            reminder = dictionary[key]
            if reminder["author"] == author:

                reminders[key] = reminder

        return reminders

    @staticmethod
    def get_reminder_by_hour(hour):

        reminders = {}
        dictionary = ReminderManager.DATA_TOOL.get_dictionary()
        # Retrieve all the reminders of the given hour
        for key in dictionary:

            reminder = dictionary[key]
            if reminder["hour"] == hour:

                reminders[key] = reminder

        return reminders

    @staticmethod
    def get_my_daily_reminder(author):

        reminders = {}
        author = "<@" + author + ">"
        dictionary = ReminderManager.DATA_TOOL.get_dictionary()
        # Retrieve all the daily reminders where the user is inside

        for key in dictionary:
            reminder = dictionary[key]

            if reminder["is_everyday"]:

                for user in reminder["users"]:

                    if author == user:
                        reminders[key] = reminder
        return reminders



    @staticmethod
    def remind_something_at_someone(author, content, someone):
        ReminderManager.SLACK_CLIENT.api_call(
            "chat.postMessage",
            channel=someone,
            text=author + " m'a demandé de vous rappeler: \"" + content + "\""
        )

    @staticmethod
    def print_my_reminder(author, print_daily):

        if not print_daily:
            reminders = ReminderManager.get_reminder_by_author(author)

            if len(reminders) > 0:
                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="Voici vos rappels :wink:",
                    attachments=[]
                )
                response_structure = []
                for key in reminders:
                    reminder = reminders[key]
                    everyday = " "
                    if reminder["is_everyday"]:
                        everyday = " tous les jours "
                    response_structure.append({
                            "text": ":thumbsup: Je rappellerai" + everyday + "à " + ", ".join(reminder["users"]) + ": " + str(reminder["content"]),
                            "callback_id": "display_my_reminders",
                            "color": "#3AA3E3",
                            "attachment_type": "default",
                            "actions": [
                                {
                                    "name": "delete",
                                    "text": "Supprimer",
                                    "type": "button",
                                    "value": key
                                }
                            ]
                        }
                    )

                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="",
                    attachments=response_structure
                )
            else:
                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="Vous n'avez pas de rappels :confused:\n Tapez `help` pour voir les commandes de rappel :blush:",
                    attachments=[]
                )
        else:

            reminders = ReminderManager.get_my_daily_reminder(author)

            if len(reminders) > 0:
                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="Voici les rappels quotidiens que des utilisateurs ont défini pour vous",
                    attachments=[]
                )
                response_structure = []
                for key in reminders:
                    reminder = reminders[key]
                    response_structure.append({
                        "text": "<@" + reminder["author"] + "> vous rappel tous les jours à " + reminder["hour"] + ": " + str(reminder["content"]),
                        "callback_id": "display_my_reminders",
                        "color": "#3AA3E3",
                        "attachment_type": "default",
                        "actions": [
                            {
                                "name": "delete_daily",
                                "text": "Supprimer (:warning: notifie l'auteur)",
                                "type": "button",
                                "value": json.dumps([key, reminder["author"], author, str(reminder["content"])])
                            }
                        ]
                    })

                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="",
                    attachments=response_structure
                )
            else:
                ReminderManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=author,
                    text="Personne n'a configuré de rappels quotidiens sur vous :wink:",
                    attachments=[]
                )