# coding: utf-8
import os
import json
import uuid
import re
import threading, time
from time import gmtime, strftime
from slackclient import SlackClient

class DataTool:

    FILE = None

    DICTIONARY = None

    def __init__(self, filename):

        # Init the datatool with the dictionary if it exists

        if os.path.isfile(filename):
            self.FILE = filename

        if os.stat(self.FILE).st_size != 0:
            with open(self.FILE) as f:
                self.DICTIONARY = json.load(f)
        else:
            self.DICTIONARY = {}

    def save_dictionary(self):

        # Sync the self.File dictionary with the json object inside the document

        open(self.FILE, 'w').close()
        with open(self.FILE, 'r+') as f:
            if len(self.DICTIONARY) > 0:
                f.write(json.dumps(self.DICTIONARY))
            else:
                f.write("")


    def delete_key(self, key):

        # Delete a specific key
        result = self.DICTIONARY.pop(key, None)
        self.save_dictionary()
        return result

    def add_key(self, values):

        # Update the dictionnary with a new reminder
        new_key = str(uuid.uuid4())
        self.DICTIONARY[new_key] = values
        self.save_dictionary()
        return new_key

    def update_key(self, key, values):

        # Update the dictionnary with a new reminder
        self.DICTIONARY[key] = values
        self.save_dictionary()

    def get_dictionary(self):

        # Get the dictionary
        return self.DICTIONARY

    def author_to_channel(self, author):

        # Parse a user tag into a channel
        matches = re.match(r'^<@(.*)>$', author)
        if matches:
            return matches.group(1)
        else:
            return None

    def channel_to_author(self, channel):

        # Parse a channel into a user tag
        return "<@"+channel+">"