# coding: utf-8

import json
import threading, time
import datetime

from time import strftime
from slackclient import SlackClient

from model.DataTool import DataTool

class PollManager:

    POLL_FILE = "model/data/poll.json"

    POLL_REGEX = "sondage\s((<@(|[WU].+?)>\s?)*)((([^;]*);?){2,})"

    REMINDER_DICTIONNARY = None

    SLACK_CLIENT = None

    DATA_TOOL = DataTool(POLL_FILE)

    @staticmethod
    def init_poll_process(slack_client):

        PollManager.SLACK_CLIENT = slack_client


    @staticmethod
    def create_poll(regexp_matches, author):

        users_mentionned = regexp_matches.group(1).split(" ")[0:-1]
        poll_info = regexp_matches.group(4).split(";")

        if ("<@" + author + ">") not in users_mentionned:
            users_mentionned.append("<@" + author + ">")

        response = ""

        options = {}
        results = {}

        if len(poll_info) > 1:

            poll_title = poll_info[0].strip()
            i = 0
            for option in poll_info[1:]:
                options[i] = option
                i = i+1

            for user in users_mentionned:
                results[user]=-1

            poll_id = PollManager.DATA_TOOL.add_key({
                "author": author,
                "users": users_mentionned,
                "title": poll_title,
                "poll_options": options,
                "result": results
            })


            status_code = PollManager.send_poll(poll_id)

            if status_code == 0:
                response = "Votre sondage a bien été créé :smile: :newspaper: "
            else:
                response = "Votre rappel a été créé mais une erreur s'est produite lors de l'envoi des notifications :cry:"

        else:
            response = "Il manque des informations pour initier votre sondage :cry:\nTapez `help` pour vérifier la syntaxe !"



        return response

    @staticmethod
    def get_regexp_validator():
        return PollManager.POLL_REGEX

    @staticmethod
    def send_poll(poll_id):

        status_code = 0
        poll = PollManager.get_poll_by_id(poll_id)

        if poll:

            author = PollManager.DATA_TOOL.channel_to_author(poll["author"])
            attachment = [
                {
                    "text": poll["title"],
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "poll_choice",
                    "actions": []
                }
            ]
            options = poll["poll_options"]

            for i in poll["poll_options"]:

                attachment[0]["actions"].append(
                    {
                        "name": "choice",
                        "text": options[i],
                        "type": "button",
                        "value": json.dumps([poll_id, i])
                    }
                )
            print(attachment)
            for user in poll['users']:

                PollManager.SLACK_CLIENT.api_call(
                    "chat.postMessage",
                    channel=PollManager.DATA_TOOL.author_to_channel(user),
                    text=author + " a lancé un sondage: ",
                    attachments=attachment
                )
        else:
            status_code = 1

        return status_code

    @staticmethod
    def get_poll_by_id(poll_id):

        poll = None
        dictionary = PollManager.DATA_TOOL.get_dictionary()
        # Retrieve all the reminders of the given author
        for key in dictionary:

            if key == poll_id:
                poll = dictionary[key]

        return poll