# coding: utf-8

import os
import time
import re
import json
import threading
import uuid
from flask import Flask, request, make_response, Response
from slackclient import SlackClient
from model.ReminderManager import ReminderManager
from model.PollManager import PollManager

# instantiate Slack client
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))

# Flask app
app = Flask(__name__)

# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

AVAILABLE_COMMAND = ["rappel", "retro", "sondage", "help", "mes rappels"]



def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        print(event)
        if event["type"] == "message" and not "subtype" in event:
            return event["text"], event["channel"], event["user"]

    return None, None, None



def handle_command(command, channel, author):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Je n'ai pas compris ce que vous vouliez dire :cry:\nTapez `help` pour plus d'informations "

    # Finds and executes the given command, filling in response
    response = None

    input = command.lower()

    # This is where you start to implement more commands!
    if input.startswith(tuple(AVAILABLE_COMMAND)):

        if input.startswith("rappel"):
            command = command.encode("utf-8", errors="ignore")
            matches = re.search(ReminderManager.get_regexp_validator(), command.decode('utf-8'))

            if matches:
                response = ReminderManager.create_reminder(matches, author)
            else:
                response = "Désolé, je ne comprends pas votre rappel :cry:, tapez `help` pour vérifier la syntaxe "

        elif input.startswith("mes rappels"):

            if "quotidien" not in input:

                ReminderManager.print_my_reminder(author, print_daily=False)
                response = ""
            else:

                ReminderManager.print_my_reminder(author, print_daily=True)
                response = ""

        elif input.startswith("sondage"):
            command = command.encode("utf-8", errors="ignore").decode('utf-8')
            matches = re.search(PollManager.get_regexp_validator(), command)

            if matches:
                response = PollManager.create_poll(matches, author)
            else:
                response = "Désolé, je ne comprends pas votre sondage :cry:, tapez `help` pour vérifier la syntaxe "

        elif input.startswith("help"):

            response = "Essayez les commandes suivantes:\
                \n• rappel `@nom1` [`@nom2` `@nom3` etc. ] `mon rappel` `à ..h.. [tous les jours]` : _Définit un rappel pour un groupe de personnes donné_\
                \n• mes rappels : _Affiche les rappels que *vous* avez programmé_\
                \n• mes rappels quotidiens : _Affiche les rappels que *les autres* on programmé tous les jours_\
                \n• retro : _Donne des idées de retrospectives_\
                \n• sondage `option1` `option2` ..."


    if response is not "":
        # Sends the response back to the channel
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=response or default_response
        )


@app.route('/', methods=['GET'])
def test():
    return Response('It works!')

@app.route("/slack/message_actions", methods=["POST"])
def message_actions():

    # Parse the request payload
    form_json = json.loads(request.form["payload"])
    result = make_response("", 200)
    print(form_json)
    if form_json["callback_id"] == "display_my_reminders":

        if form_json["actions"][0]["name"] == "delete":

            # DELETE REMINDER

            reminder_id = form_json["actions"][0]["value"]
            result_reminder_removal = ReminderManager.remove_reminder_by_id(reminder_id)

            original_attachment = form_json["original_message"]['attachments']

            # Rebuild the message
            for attachment in original_attachment:
                if "actions" in attachment and attachment["actions"][0]["value"] == reminder_id:
                    attachment.pop("actions")
                    attachment["text"] = "Rappel supprimé !"

            if result_reminder_removal:
                slack_client.api_call(
                    "chat.update",
                    channel=form_json["channel"]["id"],
                    ts=form_json["message_ts"],
                    text="Voici vos rappels :wink:",
                    attachments=original_attachment
                )
            else:
                result = make_response("Erreur de suppression du rappel", 402)

        elif form_json["actions"][0]["name"] == "delete_daily":

            # DELETE DAILY REMINDER

            data = json.loads(form_json["actions"][0]["value"])
            # ID of the deleted daily reminder
            reminder_id = data[0]

            # Original author to notify
            author_to_notify = data[1]

            # User who deleted is daily reminder
            author = "<@"+data[2]+">"

            result_reminder_removal = ReminderManager.remove_reminder_by_id(reminder_id)

            original_attachment = form_json["original_message"]['attachments']

            # Rebuild the message
            for attachment in original_attachment:
                if "actions" in attachment and data[0] == reminder_id:
                    attachment.pop("actions")
                    attachment["text"] = "Rappel supprimé !"

            if result_reminder_removal:
                slack_client.api_call(
                    "chat.update",
                    channel=form_json["channel"]["id"],
                    ts=form_json["message_ts"],
                    text="Voici vos rappels :wink:",
                    attachments=original_attachment
                )
                slack_client.api_call(
                    "chat.postMessage",
                    channel=author_to_notify,
                    text=author + " a supprimé son rappel quotidien: " + data[3]
                )
            else:
                result = make_response("Erreur de suppression du rappel", 402)

    return result


def start_app():

    # Start the flask app for root management
    app.run()

if __name__ == "__main__":

    if slack_client.rtm_connect(with_team_state=False):

        print("Starter Bot connected and running!")

        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]

        ReminderManager.init_reminder_process(slack_client)
        PollManager.init_poll_process(slack_client)

        t = threading.Thread(target=start_app)
        t.daemon = True
        t.start()

        while True:
            command, channel, author = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel, author)
            time.sleep(1)
    else:
        print("Connection failed. Exception traceback printed above.")
